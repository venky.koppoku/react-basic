import { TableComponent } from "../views/TableComponent";
import { Header } from "../views/Header";
import { FilterView } from "../views/FilterForm";
import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import axios from "axios";

const MaterialInfo = {
  data: [
    {
      id: 11,
      material_name: "M1",
      weight: 14000,
      date: "13-Nov-2020",
      time: "10:42:00 AM"
    },
    {
      id: 12,
      material_name: "M2",
      weight: 14000,
      date: "13-Nov-2020",
      time: "03:42:00 PM"
    },
    {
      id: 13,
      material_name: "M1",
      weight: 14000,
      date: "13-Nov-2020",
      time: "10:42:00 AM"
    },
    {
      id: 14,
      material_name: "M3",
      weight: 14000,
      date: "13-Nov-2020",
      time: "03:42:00 PM"
    },
    {
      id: 14,
      material_name: "M2",
      weight: 14000,
      date: "13-Nov-2020",
      time: "03:42:00 PM"
    }
  ],
  count: 2
};

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  }
}));

export const DashBoardPagePage = props => {
  const classes = useStyles();
  const [data, setData] = useState([]);
  const [count, setCount] = useState(0);
  const [filterInfo, setFilterInfo] = useState({ filter1: "", filter2: "" });

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const handleChangePage = (event, newPage) => {
    setPage(+JSON.parse(newPage));
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  //const [filterData, setFilteredData] = useState(data);
  let url = `http://ec08e5250d5a.ngrok.io/?pageSize=${rowsPerPage}&pageIndex=${page}`;
  if (filterInfo["filter1"] !== "" && filterInfo["filter2"] === "") {
    url = `${url}&materialName=${filterInfo["filter1"]}`;
  } else if (filterInfo["filter1"] !== "" && filterInfo["filter2"] === "") {
    url = `${url}&date=${filterInfo["filter2"]}`;
  } else if (filterInfo["filter1"] !== "" && filterInfo["filter2"] !== "") {
    url = `${url}&materialName=${filterInfo["filter1"]}&date=${
      filterInfo["filter2"]
    }`;
  } else {
    url = url;
  }

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios(url);

      setData(result.data.data);
      setCount(result.data.count);
    };

    fetchData();
  }, [filterInfo, page, rowsPerPage]);

  const filterSubmit = newValue => {
    console.log(newValue);
    setFilterInfo({ ...filterInfo, ...newValue });
  };

  //   useEffect(() => {
  //     //fetchData();
  //     console.log(filterInfo);
  //     const fData = data.filter(
  //       x => x["material_name"] === filterInfo["filter1"]
  //     );
  //     console.log(fData);
  //     setFilteredData(fData);
  //   }, [filterInfo]);

  return (
    <div className={classes.root}>
      <Grid container>
        <Grid item xs={3}>
          <Paper className={classes.paper}>
            <FilterView data={data} filterSubmit={filterSubmit} />
          </Paper>
        </Grid>
        <Grid item xs>
          <Paper className={classes.paper}>
            <Header props={props} />
          </Paper>
          <br />
          <br />
          <Paper className={classes.paper}>
            <TableComponent
              data={data}
              count={count}
              page={page}
              rowsPerPage={rowsPerPage}
              handleChangePage={handleChangePage}
              handleChangeRowsPerPage={handleChangeRowsPerPage}
            />
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};

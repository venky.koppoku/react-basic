import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { LoginPage } from "../pages/LoginPage";
import { DashBoardPagePage } from "../pages/DashBoard";

export const AppRouter = () => (
  <Router>
    <Switch>
      <Route path="/" exact={true} component={LoginPage} />
      <Route path="/dashboard" component={DashBoardPagePage} />
    </Switch>
  </Router>
);
